@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <img src="/image/EAD.png" class="rounded-circle" style="width:6%; margin-right:1%;" alt="profil">
                    {{ Auth::user()->name }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="text-center">
                        <img src="/image/th.jpg" style="width:6%; margin-right:1%;" alt="Telkom">
                    </div>
                    <div class="card-header">
                        <img src="/image/abc.png" class="rounded-circle" style="width:6%; margin-right:1%;" alt="Telkom">
                        {{ Auth::user()->name }}</div>
                        Telkom University
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection