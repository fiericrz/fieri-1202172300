<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id',10);
            $table->string('name',191);
            $table->string('email',191);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password',191);
            $table->string('title',191)->nullable();
            $table->text('description',191)->nullable();
            $table->string('url',191)->nullable();
            $table->string('avatar',191)->nullable();
            $table->rememberToken(100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
